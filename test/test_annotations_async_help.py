import pytest
import time

from psychopy.annotations.async_help import Async_help as Async

def test_wait_return_all_none():
	@Async()
	def f(x):
		return None

	for x in range(1,10):
		f(x)
	ret=Async().wait_finish()
	assert( not all(ret) )
	assert( len(ret)==9 )

def test_wait_return_first_true():
	@Async()
	def f(x):
		if x==1:
			return True
		return False

	for x in range(1,10):
		f(x)

	ret=Async().wait_finish()
	assert( len(ret)==9 )
	assert( ret[0] )
	ret[0]=False
	assert( not all(ret) )

def test_sleep_async():
	@Async()
	def f(x):
		time.sleep(10-x)
		return x

	for x in range(1,10):
		f(x)

	ret=Async().wait_finish()
	assert( len(ret)==9 )
	assert( ret==[ x for x in range(1,10) ] )

def test_Async_from_object():
	run_async=Async()

	@run_async
	def f(x):
		time.sleep(x)
		return x

	@run_async
	def y(x):
		time.sleep(x)
		return x

	for x in range(1,10):
		f(x)
	for x in range(9,0,-1):
		y(x)

	ret=run_async.wait_finish()
	assert( len(ret)==18 )
	assert( ret==[ x for x in range(1,10) ]+[ x for x in range(9,0,-1) ])

