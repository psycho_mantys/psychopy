import pytest

from psychopy.algorithm.is_prime import is_prime
from psychopy.annotations.time_it import Time_it

PRIMES=[1073676287, 68718952447, 274876858367, 4398042316799, 1125899839733759]

@Time_it()
def w(n):
	return is_prime(n)

@Time_it(
	log_name="Test Test",
	log_time=True,
	log_format='{NAME!r}({ARGS},{KWARGS}) :::: {TOTAL_TIME:2.2f} ms')
def p(n):
	return is_prime(n)

def test_run_time_it_funcs():
	print(p(11))
	print(p(11))
	print(p(11))
	print(Time_it()._logs)

	T=Time_it(
		log_name='Fix object',
		log_time=True,
		log_format='{NAME}({ARGS},{KWARGS}) => ({TOTAL_TIME}) ms'
	)

	@T
	def z(n):
		return is_prime(n)
	print(z(1125899839733758))
	print(Time_it()._logs)
	print(z(11))
	print(Time_it()._logs)

	print(w(11))

def test_store_time():
	@Time_it(
		log_time=True,
		log_name="prime"
	)
	def isprime(n):
		return is_prime(n)

	assert(isprime(PRIMES[1]))
	time_lesser_prime=Time_it()._logs["prime"][0]

	assert(isprime(PRIMES[3]))
	time_major_prime=Time_it()._logs["prime"][1]

	assert(time_lesser_prime<time_major_prime)

