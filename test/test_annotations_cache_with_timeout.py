import pytest
import time


from psychopy.algorithm.is_prime import is_prime
from psychopy.annotations.time_it import Time_it
from psychopy.annotations.cache_with_timeout import Cache_with_timeout

PRIMES=[1073676287, 68718952447, 274876858367, 4398042316799, 1125899839733759]

cwt=Cache_with_timeout(60*60)

def test_run_cache_with_timeout():
 
	@cwt
	def square(arg):
		return arg*arg

	@cwt
	def isprime(n):
		return is_prime(n)

	print(square(10))
	print(square(10))
	print(square(10))
	print(square(10))
	print(square(11))
	print(square(1000))
	print(Cache_with_timeout()._caches)
	time.sleep(2)
	Cache_with_timeout().collect()
	print(Cache_with_timeout()._caches)
	print(square(10))
	print(square(10))
	print(square(10))
	print(square(11))
	print(Cache_with_timeout()._caches)
	Cache_with_timeout().collect()
	print(Cache_with_timeout()._caches)

	print(isprime(PRIMES[0]))
	print(isprime(PRIMES[0]))

def test_cache_exists():
	@Cache_with_timeout(60*60)
	def isprime(n):
		return is_prime(n)

	print(isprime(PRIMES[0]))
	assert(len(Cache_with_timeout()._caches)>0)


def test_verify_timed_cache():
	@Time_it(
		log_time=True,
		log_name="prime"
	)
	@Cache_with_timeout(60*60)
	def isprime(n):
		return is_prime(n)

	assert(isprime(PRIMES[3]))
	time_without_cache=Time_it()._logs["prime"][0]

	assert(isprime(PRIMES[3]))
	time_with_cache=Time_it()._logs["prime"][1]


	print(Time_it()._logs)

	assert(time_with_cache<time_without_cache)

