import pytest

from psychopy.algorithm.is_prime import is_prime
from psychopy.annotations.print_traceback import Print_traceback

@Print_traceback()
def w(n):
	return is_prime(n)

@Print_traceback()
def r():
	raise RuntimeError("XFAIL")

def test_run_without_raise():
	print(w(11))

def test_run_raising():
	try:
		r()
	except RuntimeError as e:
		assert(str(e)=="XFAIL")
		return True
	else:
		assert(False)

