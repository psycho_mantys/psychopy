import pytest

from psychopy.algorithm.is_prime import is_prime
from psychopy.annotations.print_scope import Print_scope

@Print_scope()
def w(n):
	return is_prime(n)

@Print_scope()
def r():
	raise RuntimeError("XFAIL")

def test_run_without_raise():
	print(w(11))

def test_run_raising():
	try:
		r()
	except RuntimeError as e:
		assert(str(e)=="XFAIL")
		return True
	else:
		assert(False)

