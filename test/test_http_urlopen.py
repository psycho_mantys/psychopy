import pytest

from psychopy.http.urlopen_help import Urlopen_help

def test_interface_session():
	ka=Urlopen_help()
	assert(ka.urlopen_session("http://google.com"))
	assert(ka.urlopen_session("http://ifconfig.co").read())

	assert(ka.urlopen_session("http://ifconfig.co",headers={}).read())
	assert(ka.urlopen_session("http://ifconfig.co",headers={"opa":"qqq"}).read())

	assert(ka.urlopen_session("http://ifconfig.co",data=None).read())

	assert(ka.urlopen_session("http://ifconfig.co",timeout=30).read())
	assert(ka.urlopen_session("http://ifconfig.co",timeout=None).read())
	assert(ka.urlopen_session("http://ifconfig.co",timeout="Opa").read())



"""
def test_interface_nosession():
	ka=Urlopen_help()
	assert(ka.urlopen_nosession("http://google.com"))
	assert(ka.urlopen_nosession("http://ifconfig.co/").read())

	assert(ka.urlopen_nosession("http://ifconfig.co",headers={}).read())
	assert(ka.urlopen_nosession("http://ifconfig.co",headers={"opa":"qqq"}).read())

	assert(ka.urlopen_nosession("http://ifconfig.co",data=None).read())

	assert(ka.urlopen_nosession("http://ifconfig.co",timeout=30).read())
	assert(ka.urlopen_nosession("http://ifconfig.co",timeout=0).read())
	assert(ka.urlopen_nosession("http://ifconfig.co",timeout="Opa").read())
"""

def test_https():
	ka=Urlopen_help()
	#assert(ka.urlopen_nosession("http://ifconfig.co/",timeout=10).read())
	assert(ka.urlopen_session("https://google.com",timeout=20).read())



