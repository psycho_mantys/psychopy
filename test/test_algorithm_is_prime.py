import pytest

from psychopy.algorithm.is_prime import is_prime

false_primes=[
	4,
	12,
	6,
	1,
	0
]
true_primes=[
	2,
	5,
	7
]

# Tests
@pytest.mark.parametrize("prime_number", true_primes)
def test_is_prime(prime_number):
	assert(is_prime(prime_number))

@pytest.mark.parametrize("false_prime_number", false_primes)
def test_not_is_prime(false_prime_number):
	assert(not is_prime(false_prime_number))

