#!/usr/bin/env python

import time

from psychopy.time.loop_per_time import loop_per_time

def test_basic_usage():
    t=loop_per_time(3)

    print(time.time())
    next(t)
    print(time.time())
    time.sleep(5)
    next(t)
    print(time.time())
