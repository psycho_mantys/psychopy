#!/usr/bin/env python

import sys
import traceback
import types

class Print_scope(object):
	def __init__(self, output_file=sys.stdout):
		if isinstance(output_file, types.FunctionType):
			raise RuntimeError("Use @Print_scope()...")


	def __call__(self, f):
		def func(*args, **kwargs):
			print("BEGIN: "+str(f.__name__))
			f(*args, **kwargs)
			print("END: "+str(f.__name__))

		func.func_name=f.__name__
		return func

