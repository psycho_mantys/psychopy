#!/usr/bin/env python

import sys
import traceback
import types

class Print_traceback(object):
	def __init__(self, output_file=sys.stdout):
		if isinstance(output_file, types.FunctionType):
			raise RuntimeError("Use @Print_traceback()...")

		self.output_file=output_file

	def __call__(self, f):
		def func(*args, **kwargs):
			try:
				return f(*args, **kwargs)
			except:
				traceback.print_exc(file=sys.stdout)
				raise

		func.func_name=f.__name__
		return func

