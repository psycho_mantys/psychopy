#!/usr/bin/env python

import time
#from multiprocessing import Pool
from multiprocessing.dummy import Pool

import types

class Async_help(object):
	_global_pool=Pool()
	_global_results=[]

	def __init__(self, pool=None):
		if isinstance(pool, types.FunctionType):
			raise RuntimeError("Use @Async()...")

		if pool:
			self.pool=pool
			self.results=[]
		else:
			self.pool=self._global_pool
			self.results=self._global_results

	def __call__(self, f):
		def func(*args, **kwargs):
			return self.results.append(self.pool.apply_async(f, args, kwargs))
		func.func_name=f.__name__

		return func

	def wait_finish(self):
		ret=[x.get() for x in self.results ]
		self.results.clear()
		return ret

