#!/usr/bin/env python

import time

class Time_it(object):
	_logs={}

	def __init__(self, **kwarg):
		self.log_time=kwarg.get('log_time', False)
		self.log_format=kwarg.get('log_format', '{NAME!r}({ARGS},{KWARGS}) ==> {TOTAL_TIME:2.2f} ms')
		self.log_name=kwarg.get('log_name', False)

	def __del__(self):
		if self.log_name in self._logs:
			del self._logs[self.log_name]

	def __call__(self, f):
		if not self.log_name:
			self.log_name=f.__name__.upper()

		self._logs[self.log_name]=[]

		def wrapper(*args, **kw):
			t_start=time.time()
			result=f(*args, **kw)
			t_end=time.time()

			t_total=(t_end-t_start)

			if self.log_time:
				print((t_total)*1000)
				self._logs[self.log_name].append((t_total)*1000)
			else:
				print(self.log_format.format(
					NAME=self.log_name,
					TOTAL_TIME=((t_total)*1000),
					ARGS=args,
					KWARGS=kw
				))

			return result
		wrapper.func_name=f.__name__

		return wrapper

