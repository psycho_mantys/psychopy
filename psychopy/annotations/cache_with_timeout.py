#!/usr/bin/env python
# Source: http://code.activestate.com/recipes/325905-memoize-decorator-with-timeout/#c1


import time

class Cache_with_timeout(object):
	_caches={}
	_timeouts={}

	def __init__(self,timeout=2):
		self.timeout=timeout

	def is_valid(self, cached_item, timeout=None):
		if not timeout:
			timeout=self.timeout
		return (time.time()-cached_item[1])<timeout

	def collect(self):
		for func in self._caches:
			cache={}
			for key in list(self._caches[func]):
				if not self.is_valid(self._caches[func][key], self._timeouts[func]):
					#print("INValid")
					del self._caches[func][key]
				else:
					#print("valid")
					pass

	def __call__(self, f):
		self.cache=self._caches[f]={}
		self._timeouts[f]=self.timeout

		def func(*args, **kwargs):
			kw=sorted(kwargs.items())
			key=(args, tuple(kw))
			try:
				v=self.cache[key]
				#print("cache")
				if not self.is_valid(v):
					raise KeyError
			except KeyError:
				#print("new")
				v=self.cache[key]=f(*args,**kwargs), time.time()
			return v[0]
		func.func_name=f.__name__

		return func

