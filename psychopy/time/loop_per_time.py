#!/usr/bin/env python

import time
from itertools import count

def loop_per_time(period):
	next_time=time.time()+period
	for i in count():
		now=time.time()
		to_sleep=next_time-now
		if to_sleep>0:
			time.sleep(to_sleep)
			next_time+=period
		else:
			next_time=now+period
		yield i, next_time

