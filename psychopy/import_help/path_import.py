#!/usr/bin/env python

import sys
import copy
import os
import importlib
from contextlib import contextmanager

@contextmanager
def add_to_path(p):
	old_paths=copy.copy(sys.path)
	old_modules=list(sys.modules.keys())
	old_cwd=os.getcwd()

	sys.path.insert(0, p)
	importlib.invalidate_caches()
	os.chdir(p)

	try:
		yield
	finally:
		for k in sys.path:
			if not k in old_paths:
				sys.path.remove(k)

		for k in list(sys.modules.keys()):
			if not k in old_modules:
				sys.modules.pop(k,None)

		importlib.invalidate_caches()
		os.chdir(old_cwd)

def path_import(absolute_path):
	absolute_path=os.path.abspath(absolute_path)
	'''implementation taken from https://docs.python.org/3/library/importlib.html#importing-a-source-file-directly'''
	with add_to_path(os.path.dirname(absolute_path)):
		spec=importlib.util.spec_from_file_location(absolute_path, absolute_path)
		module=importlib.util.module_from_spec(spec)
		spec.loader.exec_module(module)
		return module

if __name__ == "__main__":
	pass

