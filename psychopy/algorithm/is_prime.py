
def is_prime(n):
	n=abs(int(n))
	if n<2:
		return False
	if n==2: 
		return True    
	if not n&1: 
		return False

	# range starts with 3 and only needs to go up 
	# the square root of n for all odd numbers
	for x in range(3, int(n**0.5)+1, 2):
		if n%x==0:
			return False

	return True

if __name__ == '__main__':
	print(is_prime(1125899839733758))

