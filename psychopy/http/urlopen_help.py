#!/usr/bin/env python

from http.client import HTTPConnection, HTTPSConnection, CannotSendRequest
from urllib.request import urlopen, Request

import select

def list_get(l, idx, default=None):
	try:
		return l[idx]
	except IndexError:
		return default

class Urlopen_help():
	def __init__(self):
		self.__connections={}

	def reset_connection_pool(self):
		for key, value in self.__connections.items():
			value.close()
		self.__connections={}

	def request(self, method, url, data=None, timeout=30, headers={}):
		#scheme, _, host, path=url.split('/', 3)

		uri=url.split('/', 3)
		scheme=list_get(uri, 0)
		host=list_get(uri, 2)
		path=list_get(uri, 3, "")

		h=self.__connections.get((scheme, host))
		if h:
			if not h.sock and select.select([h.sock], [], [], 0)[0]:
				h.close()
				h=None
		if not ( h and h.sock ):
			Connection=HTTPConnection if scheme=='http:' else HTTPSConnection
			h=self.__connections[(scheme, host)]=Connection(host, timeout=timeout)
		h.request(method, '/' + path, data, headers)
		return h.getresponse()

	def urlopen_session(self, url, timeout=30, data=None, headers={}):
		resp=self.request('POST' if data else 'GET', url, data=data, timeout=timeout, headers=headers)
		assert resp.status<400, (resp.status, resp.reason, resp.read())
		return resp

	"""
	def urlopen_nosession(self, url, timeout=30, data=None, headers={}):
		webpage=urlopen(
			Request(
				url,
				data=data,
				headers=headers
			), timeout=timeout )
		print(url)
		print(timeout)
		print(data)
		print(headers)
		return webpage
	"""

if __name__ == '__main__':
	ka=Urlopen_help()
	print(ka.urlopen_session("http://google.com").read())
	"""assert(ka.urlopen_nosession("http://google.com", timeout=30, headers={'User-Agent': 'Mozilla/5.0'}, data=b' ').read())"""

