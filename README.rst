psychopy
======================================

Python library from Psycho Mantys


Using from github
--------------------------------------

If you use pipenv, you can do:

.. code:: bash

	pipenv install pipenv
	pipenv install git+https://bitbucket.org/psycho_mantys/psychopy.git#egg=psychopy


